from transformers.utils import logging
from .model import Model
from .storage_handler import StorageHandler
from transformers import GPTNeoForCausalLM, GPT2Tokenizer
import logging
import sys
import argparse


parser = argparse.ArgumentParser(
    description='Log level via argparse'
)
parser.add_argument("-v", "--verbose", help="increase output verbosity",
                    action="store_true")

args = parser.parse_args()
logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s [%(levelname)s] %(message)s",
        handlers=[
            logging.FileHandler("debug.log"),
            logging.StreamHandler(sys.stdout)
        ]
    )
if args.verbose:
    logging.basicConfig(level=logging.DEBUG)



class ModelObserver:
   def __init__(self):
       self.observers = []
       self.counter = 0

   def add(self, observer):
       if observer not in self.observers:
           self.observers.append(observer)
       else:
           logging.info('Failed to add: {}'.format(observer))

   def remove(self, observer):
       try:
           self.observers.remove(observer)
       except ValueError:
           logging.info('Failed to remove: {}'.format(observer))

   def notify(self):
       self.counter += 1
       for o in self.observers:
           logging.info("Message to {}: Called {} times".format(o, self.counter))


class ExampleModel(Model):

    def __init__(self, storage_handler: StorageHandler):
        super().__init__(storage_handler)

    def talk(self, user_input: str, observer ) -> str:
        observer.notify()
        model = GPTNeoForCausalLM.from_pretrained("EleutherAI/gpt-neo-125M")
        tokenizer = GPT2Tokenizer.from_pretrained("EleutherAI/gpt-neo-125M")

        #prompt = "For writers, a random sentence can help them get their creative juices flowing."

        input_ids = tokenizer(user_input, return_tensors="pt").input_ids

        bad_words = [tokenizer.encode(bad_word, add_prefix_space=True) for bad_word in ['cool']]
            
        gen_tokens = model.generate(input_ids, do_sample=True,bad_words_tokens=bad_words, temperature=0.9, max_length=150,)
        gen_text = tokenizer.batch_decode(gen_tokens)[0]

        gen_text = gen_text.replace(user_input, "")


        gen_text, sep, tail = gen_text.partition('\n')
        return gen_text

    

    def get_content(self):
        pass
        #return "Some text, some more text, actually a whole conversation of an example model vers. {}...".format(self._version)
