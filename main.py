from logging import setLoggerClass
from os.path import expanduser

from models.example_model import ExampleModel, ModelObserver
from models.model import Model
from models.storage_handler import StorageHandler

import logging
import argparse
import sys

parser = argparse.ArgumentParser(
    description='Log level via argparse'
)
parser.add_argument("-v", "--verbose", help="increase output verbosity",
                    action="store_true")

args = parser.parse_args()
logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s [%(levelname)s] %(message)s",
        handlers=[
            logging.FileHandler("debug.log"),
            logging.StreamHandler(sys.stdout)
        ]
    )
if args.verbose:
    logging.basicConfig(level=logging.DEBUG)




def start(GPTNeoModel, observer):

    counter = 0

    while True:

        user_input = input("Max:")
        if user_input.lower() == "bye":
            break
        response = GPTNeoModel.talk(user_input, observer)
        templateSentence = "If I'm president in 2024 then"
        if(counter == 2):
            response = templateSentence + response
        logging.info("Donald:{}".format(response))
        counter += 1

    GPTNeoModel.dump()


if __name__ == "__main__":

    observer = ModelObserver()
    observer.add("Observer 1")
    observer.add("Observer 2")
    observer.remove("Observer 2")

    sh = StorageHandler(expanduser("~"))
    # TODO: change the model to your implementation
    m = ExampleModel(sh)
    start(m, observer)
    
    
